
SRC = $(wildcard $(NHDIR)source/*.cpp)
OBJ = $(patsubst $(NHDIR)source/%.cpp, %.o, $(SRC))
DEP = $(patsubst $(NHDIR)source/%.cpp, %.d, $(SRC))


SO = libFEWarpImageConstraint_$(PLAT).$(SFX)
LIB = $(NHDIR)build/lib/$(SO)

FECORE = $(FEBLIB)/libfecore_$(PLAT)64.a

FEBIOMECH = $(FEBLIB)/libfebiomech_$(PLAT)64.a

FEBIOLIBS = $(FEBIOMECH) $(FECORE)

FECORE_OSX = $(FEBLIB)/libFECore.a

FEBIOMECH_OSX = $(FEBLIB)/libFEBioMech.a

FEBIOLIBS_OSX = $(FEBIOMECH_OSX) $(FECORE_OSX)

$(LIB): $(OBJ)
ifeq ($(findstring lnx,$(PLAT)),lnx)
		$(CC) $(LNKFLG) -shared -Wl,-soname,$(SO) -o $(LIB) $(OBJ) $(FEBIOLIBS)
else ifeq ($(findstring gcc,$(PLAT)),gcc)
		$(CC) $(LNKFLG) -shared -Wl,-soname,$(SO) -o $(LIB) $(OBJ) $(FEBIOLIBS)
else
		$(CC) -dynamiclib $(FLG) -o $(LIB) $(OBJ) $(FEBIOLIBS_OSX)
endif

%.o: $(NHDIR)source/%.cpp
	$(CC) $(INC) $(DEF) $(FLG) -MMD -c -o $@ $<

clean:
	$(RM) *.o *.d $(LIB)

-include $(DEP)
