#pragma once
#include <FECore/Image.h>
#include <FECore/vec3d.h>
#include <FECore/mat3d.h>
#include "math.h"

#include <stdio.h>

static const double PI = 3.141592653589793;
// #define PI 3.141592654
// #define PI 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348
// #define PI2 = 6.2831853071795864769252867665590057683943387987502116419498891846156328125724179972560696

class PhaseMap
{
public:
	struct POINT
	{
		int		i, j, k;
		double	h[8];
		
		// modified for wrapped phase interpolation
		double r, s, t;
	};

public:
	PhaseMap(Image& img);
	~PhaseMap(void);

	void SetRange(vec3d r0, vec3d r1);

	// map a vector to the image domain
	POINT map(vec3d p);

	// evaluate image
	bool outflag(const vec3d& r); 
	double value(const POINT& p);
	double value(const vec3d& r) { return OutCast(r); }
	//double value(const vec3d& r) { return value(map(r)); }

	// phase image gradient
	vec3d gradient(const vec3d& r);

	// image hessian
	mat3ds hessian(const vec3d& r);

	// pixel dimensions
	double dx() { return (m_r1.x - m_r0.x)/(double) (m_img.width () - 1); }
	double dy() { return (m_r1.y - m_r0.y)/(double) (m_img.height() - 1); }
	double dz() { int nz = m_img.depth(); if (nz == 1) return 1.0; else return (m_r1.z - m_r0.z)/(double) (m_img.depth () - 1); }

protected:
	double OutCast(const vec3d& r); 

	double wrap(double phase); 

	double grad_x(int i, int j, int k);
	double grad_y(int i, int j, int k);
	double grad_z(int i, int j, int k);

	double wgrad_x(int i, int j, int k);
	double wgrad_y(int i, int j, int k);
	double wgrad_z(int i, int j, int k);

protected:
	Image&	m_img;
	vec3d	m_r0;
	vec3d	m_r1;	
};