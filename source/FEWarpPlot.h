#pragma once
#include "FECore/FEPlotData.h"
#include "FEWarpImageConstraint.h"
#include <FECore/NodeDataRecord.h>
#include <FECore/ElementDataRecord.h>

//-----------------------------------------------------------------------------
class FEPlotPhaseTemplate : public FENodeData
{
public:
	FEPlotPhaseTemplate(FEModel* pfem) : FENodeData(PLT_VEC3F, FMT_NODE), m_pfem(pfem) {}
	virtual bool Save(FEMesh& m, vector<float>& a);

protected:
	FEModel*	m_pfem;
	double wrap(double phase);
};

// template tagged images
class FEPlotTagTemplate : public FENodeData
{
public:
	FEPlotTagTemplate(FEModel* pfem) : FENodeData(PLT_VEC3F, FMT_NODE), m_pfem(pfem) {}
	virtual bool Save(FEMesh& m, vector<float>& a);

protected:
	FEModel*	m_pfem;
	double wrap(double phase);
	// vector<int>	m_dom;		// list of domains to warp MGTI
};

// target phase images
class FEPlotPhaseTarget : public FENodeData
{
public:
	FEPlotPhaseTarget(FEModel* pfem) : FENodeData(PLT_VEC3F, FMT_NODE), m_pfem(pfem) {}
	virtual bool Save(FEMesh& m, vector<float>& a);

protected:
	FEModel*	m_pfem;
	double wrap(double phase);
};

// target tagged images
class FEPlotTagTarget : public FENodeData
{
public:
	FEPlotTagTarget(FEModel* pfem) : FENodeData(PLT_VEC3F, FMT_NODE), m_pfem(pfem) {}
	virtual bool Save(FEMesh& m, vector<float>& a);

protected:
	FEModel*	m_pfem;
	double wrap(double phase);
};

// tag tracking force
class FEPlotForce : public FENodeData
{
public:
	FEPlotForce(FEModel* pfem) : FENodeData(PLT_VEC3F, FMT_NODE), m_pfem(pfem) {}
	virtual bool Save(FEMesh& m, vector<float>& a);

protected:
	FEModel*	m_pfem;
	double wrap(double phase);
};

//-----------------------------------------------------------------------------
// plot residual
class FEPlotPhaseResidual : public FENodeData
{
public:
	FEPlotPhaseResidual(FEModel* pfem) : FENodeData(PLT_FLOAT, FMT_NODE), m_pfem(pfem) {}
	virtual bool Save(FEMesh& m, vector<float>& a);

protected:
	FEModel*	m_pfem;
	double wrap(double phase);
};

//-----------------------------------------------------------------------------
class FELogNodePhaseResidual : public FENodeLogData
{
public:
	FELogNodePhaseResidual(FEModel* pfem) : FENodeLogData(pfem){}
	double value(int node);
protected:
        double wrap(double phase);
};

//-----------------------------------------------------------------------------
class FELogElemPhaseResidual : public FELogElemData
{
public:
	FELogElemPhaseResidual(FEModel* pfem) : FELogElemData(pfem){}
	double value(FEElement& el);
protected:
        double wrap(double phase);
};
