// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include "FECore/FECoreKernel.h"
#include "FECore/FECoreFactory.h"
#include "FECore/Logfile.h"

#include "FEWarpImageConstraint.h"
#include "FEWarpPlot.h"

#ifdef WIN32
	#define DLL_EXPORT __declspec(dllexport)
#else
	#define DLL_EXPORT
#endif

#ifdef WIN32
#include "targetver.h"
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <windows.h>

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

#ifdef RegisterClass
#undef RegisterClass
#endif

#endif // WIN32

//-----------------------------------------------------------------------------
//! Factory classes for this plugin.
FEPluginFactory_T<FEWarpImageConstraint  , FENLCONSTRAINT_ID    > warp_image_factory	     ("HARP-FE"           );
FEPluginFactory_T<FEPlotForce            , FEPLOTDATA_ID        > plot_force_factory         ("harpFE-TrackForce"    );
FEPluginFactory_T<FEPlotPhaseTemplate	 , FEPLOTDATA_ID	> plot_PhaseTemplate_factory ("harpFE-PhaseTemplate" );
FEPluginFactory_T<FEPlotPhaseTarget	 , FEPLOTDATA_ID	> plot_PhaseTarget_factory   ("harpFE-PhaseTarget"   );
FEPluginFactory_T<FEPlotPhaseResidual	 , FEPLOTDATA_ID	> plot_PhaseResidual_factory ("harpFE-PhaseResidual" );
FEPluginFactory_T<FEPlotTagTemplate	 , FEPLOTDATA_ID	> plot_TagTemplate_factory   ("harpFE-TagTemplate"   );
FEPluginFactory_T<FEPlotTagTarget	 , FEPLOTDATA_ID	> plot_TagTarget_factory     ("harpFE-TagTarget"     );
FEPluginFactory_T<FELogNodePhaseResidual , FENODELOGDATA_ID     > log_NodePhaseResidual_factory  ("PR");

//-----------------------------------------------------------------------------
// keep a copy of the FECoreKernel
FECoreKernel*	pFEBio;

//-----------------------------------------------------------------------------
FECORE_EXPORT unsigned int GetSDKVersion()
{
	return FE_SDK_VERSION;
}

//-----------------------------------------------------------------------------
extern "C" DLL_EXPORT void PluginInitialize(FECoreKernel& febio)
{
	pFEBio = &febio;
}

//-----------------------------------------------------------------------------
extern "C" DLL_EXPORT int PluginNumClasses()
{
	return 8;
}

//-----------------------------------------------------------------------------
extern "C" DLL_EXPORT FECoreFactory* PluginGetFactory(int i)
{
	
	switch (i)
	{
	case 0: return &warp_image_factory;
	case 1: return &plot_force_factory;
	case 2: return &plot_PhaseTemplate_factory;
	case 3: return &plot_PhaseTarget_factory;
	case 4: return &plot_PhaseResidual_factory;
	case 5: return &plot_TagTemplate_factory;
	case 6: return &plot_TagTarget_factory;
        case 7: return &log_NodePhaseResidual_factory;

	default:
		return 0;
	}
	
}

//-----------------------------------------------------------------------------
extern "C" DLL_EXPORT void PluginCleanup()
{

}